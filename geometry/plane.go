package geometry

import (
	m "github.com/go-gl/mathgl/mgl32"
	u "gitlab.com/tapir/maptool/utils"
)

// Plane holds vector form of a plane.
type Plane struct {
	Point  m.Vec3  // Any point on the plane (x, y, z)
	Normal m.Vec3  // Unit-length normal vector of the plane
	C      float32 // A constant
}

// NewPlaneFromPoints creates a new plane from 3 non-colineer points form
// to vector form: N.P=C
func NewPlaneFromPoints(points []m.Vec3) Plane {
	p := points[0]
	v1 := points[2].Sub(p)
	v2 := points[1].Sub(p)
	n := v1.Cross(v2).Normalize()
	c := p.Dot(n)
	return Plane{p, n, c}
}

// GetOpposite gets the plane looking at the opposite direction.
func (p Plane) GetOpposite() Plane {
	pl := p
	pl.Normal = p.Normal.Mul(-1)
	pl.C = p.Point.Dot(pl.Normal)
	return pl
}

// DistanceToPoint gets signed distance from a point to a plane: D=N.P-C
func (p Plane) DistanceToPoint(point m.Vec3) float32 {
	return point.Dot(p.Normal) - p.C
}

// IsParallel returns true if two planes are parallel.
func (p Plane) IsParallel(p2 Plane) bool {
	dot := p.Normal.Dot(p2.Normal)
	return m.FloatEqualThreshold(u.Abs(dot), 1, u.Epsilon)
}

// IntersectWithSegment gets intersection point of
// a line (L=P0+t.(P1-P0)) segment and a plane.
func (p Plane) IntersectWithSegment(p0 m.Vec3, p1 m.Vec3) (m.Vec3, bool) {
	lNormal := p1.Sub(p0)
	if m.FloatEqual(lNormal.Dot(p.Normal), 0) {
		// Line and plane are parallel or they don't intersect
		return m.Vec3{}, false
	}
	t := p.Normal.Dot(p.Point.Sub(p0)) / p.Normal.Dot(lNormal)
	return lNormal.Mul(t).Add(p0), true
}
