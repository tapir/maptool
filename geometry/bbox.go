package geometry

import m "github.com/go-gl/mathgl/mgl32"

// BoundingBox is a box that encapsulates any 3D data.
type BoundingBox struct {
	Min m.Vec3
	Max m.Vec3
}
