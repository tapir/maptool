package geometry

import (
	"fmt"

	"io/ioutil"
	"os"

	m "github.com/go-gl/mathgl/mgl32"
	u "gitlab.com/tapir/maptool/utils"
)

type ClipResult uint

const (
	ClipNone ClipResult = iota
	ClipEverything
	ClipNormal
)

type Vertex struct {
	Point     m.Vec3  // Coordinates of the vertex
	distance  float32 // Distance to clipping plane
	occurence int     // Occurance in face edge vertices list
}

type Edge struct {
	Vertices []*Vertex // Extremity points of the edge
}

type Face struct {
	Edges []*Edge // Edges that bound the face
	Plane Plane   // Plane that clips the face
}

// A triangle is a specific face type with CW ordered vertices.
type Triangle struct {
	Vertices []*Vertex
	Plane    Plane
}

// A mesh holds vertex, edge and face data of a 3D object.
type Mesh struct {
	Vertices []*Vertex
	Edges    []*Edge
	Faces    []*Face
	Box      BoundingBox
}

// NewCubeMesh creates a cube with the given unit size.
// Vertices are shared among edges and faces.
func NewCubeMesh(s float32) *Mesh {
	mh := new(Mesh)

	v0 := m.Vec3{-s / 2, -s / 2, -s / 2}
	v1 := m.Vec3{s / 2, -s / 2, -s / 2}
	v2 := m.Vec3{s / 2, -s / 2, s / 2}
	v3 := m.Vec3{-s / 2, -s / 2, s / 2}
	v4 := m.Vec3{-s / 2, s / 2, -s / 2}
	v5 := m.Vec3{s / 2, s / 2, -s / 2}
	v6 := m.Vec3{s / 2, s / 2, s / 2}
	v7 := m.Vec3{-s / 2, s / 2, s / 2}

	mh.Vertices = []*Vertex{
		{v0, 0, 0}, // 0
		{v1, 0, 0}, // 1
		{v2, 0, 0}, // 2
		{v3, 0, 0}, // 3
		{v4, 0, 0}, // 4
		{v5, 0, 0}, // 5
		{v6, 0, 0}, // 6
		{v7, 0, 0}, // 7
	}

	mh.Edges = []*Edge{
		{[]*Vertex{mh.Vertices[0], mh.Vertices[1]}}, // 0
		{[]*Vertex{mh.Vertices[1], mh.Vertices[2]}}, // 1
		{[]*Vertex{mh.Vertices[2], mh.Vertices[3]}}, // 2
		{[]*Vertex{mh.Vertices[3], mh.Vertices[0]}}, // 3
		{[]*Vertex{mh.Vertices[4], mh.Vertices[5]}}, // 4
		{[]*Vertex{mh.Vertices[5], mh.Vertices[6]}}, // 5
		{[]*Vertex{mh.Vertices[6], mh.Vertices[7]}}, // 6
		{[]*Vertex{mh.Vertices[7], mh.Vertices[4]}}, // 7
		{[]*Vertex{mh.Vertices[4], mh.Vertices[0]}}, // 8
		{[]*Vertex{mh.Vertices[5], mh.Vertices[1]}}, // 9
		{[]*Vertex{mh.Vertices[6], mh.Vertices[2]}}, // 10
		{[]*Vertex{mh.Vertices[7], mh.Vertices[3]}}, // 11
	}

	mh.Faces = []*Face{
		{
			[]*Edge{mh.Edges[0], mh.Edges[9], mh.Edges[4], mh.Edges[8]},
			NewPlaneFromPoints([]m.Vec3{v7, v4, v3}), // 7 4 3
		}, // 0
		{
			[]*Edge{mh.Edges[5], mh.Edges[10], mh.Edges[1], mh.Edges[9]},
			NewPlaneFromPoints([]m.Vec3{v1, v6, v7}), // 1 6 7
		}, // 1
		{
			[]*Edge{mh.Edges[2], mh.Edges[11], mh.Edges[6], mh.Edges[10]},
			NewPlaneFromPoints([]m.Vec3{v0, v5, v6}), // 0 5 6
		}, // 2
		{
			[]*Edge{mh.Edges[3], mh.Edges[11], mh.Edges[7], mh.Edges[8]},
			NewPlaneFromPoints([]m.Vec3{v0, v3, v4}), // 0 3 4
		}, // 3
		{
			[]*Edge{mh.Edges[7], mh.Edges[6], mh.Edges[5], mh.Edges[4]},
			NewPlaneFromPoints([]m.Vec3{v4, v7, v6}), // 4 7 6
		}, // 4
		{
			[]*Edge{mh.Edges[2], mh.Edges[1], mh.Edges[0], mh.Edges[3]},
			NewPlaneFromPoints([]m.Vec3{v0, v1, v2}), // 0 1 2
		}, // 5
	}

	return mh
}

// IsEqual returns true if two vertices are equal.
func (v1 *Vertex) IsEqual(v2 *Vertex) bool {
	return v1.Point.ApproxEqualThreshold(v2.Point, u.EpsilonVertice)
}

// Winding returns a slice of vertices that are ordered CW.
func (f *Face) Winding() []*Vertex {
	var (
		currentVertex     = f.Edges[0].Vertices[0]
		vertexList        = []*Vertex{currentVertex}
		countVertexVertex = len(f.Edges)
	)

	// Walk all the edges and insert the connected vertices to a list
	for i := 0; len(vertexList) < countVertexVertex; i++ {
		i %= countVertexVertex
		for j, v := range f.Edges[i].Vertices {
			if v == currentVertex {
				currentVertex = f.Edges[i].Vertices[j^1]
				vertexList = append(vertexList, currentVertex)
				break
			}
		}
	}

	// Find the normal of the ordered vertices
	v1 := vertexList[1].Point.Sub(vertexList[0].Point)
	v2 := vertexList[2].Point.Sub(vertexList[0].Point)
	normal := v1.Cross(v2).Normalize()

	// Compare with the normal of the face and re-order if necessary
	if normal.Dot(f.Plane.Normal) < 0 {
		for i, j := 0, countVertexVertex-1; i < j; i, j = i+1, j-1 {
			vertexList[i], vertexList[j] = vertexList[j], vertexList[i]
		}
	}

	return vertexList
}

func (t1 *Triangle) IsCoplanar(t2 *Triangle) bool {
	for _, v := range t1.Vertices {
		if !m.FloatEqualThreshold(t2.Plane.DistanceToPoint(v.Point), 0, u.EpsilonVertice) {
			return false
		}
	}
	return true
}

// Compare compares vertices of two triangles and merges them if they are equal.
// Original is modified.
func (t1 *Triangle) Compare(t2 *Triangle) {
	// TODO: Optimize but not comparing every vertice.
	// A vertex can only be same with another vertex.
	if t1.IsCoplanar(t2) {
		for i := 0; i < len(t2.Vertices); i++ {
			for j := 0; j < len(t1.Vertices); j++ {
				v1 := t1.Vertices[j]
				v2 := t2.Vertices[i]
				if v1 != v2 && v1.IsEqual(v2) {
					t1.Vertices[j] = v2
					break
				}
			}
		}
	}
}

// Triangles returns the list of triangles that makes up the mesh.
// Triangles are CW ordered and vertices are duplicated for each face
// of the mesh.
func (mh *Mesh) Triangles() []*Triangle {
	var triangles []*Triangle

	for _, f := range mh.Faces {
		var (
			winding     = f.Winding()
			countVertex = len(winding)
			vertices    []*Vertex
		)

		// Copy vertices
		for _, v := range winding {
			vertices = append(vertices, &Vertex{Point: v.Point})
		}

		// Triangulate surface by consuming vertices
		pMaster := vertices[0]
		pSlave := vertices[1]
		vertices = vertices[2:]

		for countVertex = len(vertices); countVertex > 0; countVertex = len(vertices) {
			// Pop another vertice
			pTemp := vertices[0]
			vertices = vertices[1:]

			// Push new triangle to the mesh with edges and faces
			triVerts := []*Vertex{pMaster, pSlave, pTemp}
			triangles = append(triangles, &Triangle{triVerts, f.Plane})

			// On to the next
			pSlave = pTemp
		}
	}

	return triangles
}

// IsDisjoint checks if 2 meshes are intersecting or not.
// Touching planes/edges/vertices are not considered intersecting.
func (mh *Mesh) IsDisjoint(mesh *Mesh) bool {
	// If all the vertices of mesh is outside of mh's any plane
	// then they are disjoint
	for _, p := range mh.Faces {
		pos := 0
		for _, v := range mesh.Vertices {
			dist := p.Plane.DistanceToPoint(v.Point)
			if u.Less(dist, 0) {
				break
			}
			pos++
		}
		if pos == len(mesh.Vertices) {
			return true
		}
	}
	return false
}

// Copy deep-copies a mesh.
func (mh *Mesh) Copy() *Mesh {
	cpy := new(Mesh)
	cpy.Vertices = make([]*Vertex, len(mh.Vertices))
	cpy.Edges = make([]*Edge, len(mh.Edges))
	cpy.Faces = make([]*Face, len(mh.Faces))

	// First copy vertices
	for i := 0; i < len(cpy.Vertices); i++ {
		cpy.Vertices[i] = new(Vertex)
		*cpy.Vertices[i] = *mh.Vertices[i]
	}

	// Restructure edges
	for i, e := range mh.Edges {
		cpy.Edges[i] = new(Edge)
		cpy.Edges[i].Vertices = make([]*Vertex, 2)
		for j, v := range mh.Vertices {
			for k := 0; k < 2; k++ {
				if e.Vertices[k] == v {
					cpy.Edges[i].Vertices[k] = new(Vertex)
					cpy.Edges[i].Vertices[k] = cpy.Vertices[j]
				}
			}
		}
	}

	// Restructure faces
	for i, f := range mh.Faces {
		cpy.Faces[i] = new(Face)
		cpy.Faces[i].Edges = make([]*Edge, len(f.Edges))
		for j, e := range mh.Edges {
			for k, ee := range f.Edges {
				if ee == e {
					cpy.Faces[i].Edges[k] = new(Edge)
					cpy.Faces[i].Edges[k] = cpy.Edges[j]
					cpy.Faces[i].Plane = f.Plane
				}
			}
		}
	}

	return cpy
}

// Split splits a mesh and returns the pieces.
// Original is not disturbed.
func (mh *Mesh) Split(p Plane) []*Mesh {
	var (
		split []*Mesh
		neg   = mh.Copy()
		pos   = mh.Copy()
	)

	if neg.Clip(p) == ClipNormal {
		split = append(split, neg)
	} else {
		return nil
	}

	if pos.Clip(p.GetOpposite()) == ClipNormal {
		split = append(split, pos)
	} else {
		return nil
	}

	return split
}

// Subtract subtracts mesh from mh. Returns new brushes that are formed.
// Original is not disturbed.
func (mh *Mesh) Subtract(mesh *Mesh) []*Mesh {
	var (
		new      []*Mesh
		nextMesh = mh
	)

	for _, f := range mesh.Faces {
		r := nextMesh.Split(f.Plane)
		if len(r) == 2 {
			new = append(new, r[1])
			nextMesh = r[0]
		}
	}

	return new
}

// Clip clips a mesh with a plane. Considering that the plane points are in
// CW order (like in Quake map files) positive side of plane gets clipped.
// Original is modified.
func (mh *Mesh) Clip(p Plane) ClipResult {
	// Process vertices
	pos, neg := 0, 0
	for i := len(mh.Vertices) - 1; i >= 0; i-- {
		v := mh.Vertices[i]
		v.distance = p.DistanceToPoint(v.Point)

		switch {
		// Point is on the clipping plane
		case m.FloatEqualThreshold(v.distance, 0, u.Epsilon):
			v.distance = 0
			neg++

		// Point is on the positive side of the clipping plane
		// Disregard this vertice from now on
		case v.distance > 0:
			mh.Vertices = append(mh.Vertices[:i], mh.Vertices[i+1:]...)
			pos++

		// Point is on the negative side of the clipping plane
		default:
			neg++
		}
	}

	// No vertice on the positive side of the plane, nothing got clipped
	if pos == 0 {
		return ClipNone
	}

	// No vertice on the negative side of the plane, everything got clipped
	if neg == 0 {
		return ClipEverything
	}

	// Process edges
	for i := len(mh.Edges) - 1; i >= 0; i-- {
		e := mh.Edges[i]
		d0, d1 := e.Vertices[0].distance, e.Vertices[1].distance
		switch {
		// If both points on the positive side, the edge is culled.
		// ATTENTION: This also culls the edges that are on the plane as well.
		// This is needed because in face process phase, it will be recreated and this
		// will allow the new clipping face to have a reference to this edge.
		case u.Moe(d0) && u.Moe(d1):
			mh.Edges = append(mh.Edges[:i], mh.Edges[i+1:]...)
			for j := len(mh.Faces) - 1; j >= 0; j-- {
				f := mh.Faces[j]
				for k := len(f.Edges) - 1; k >= 0; k-- {
					if f.Edges[k] == e {
						f.Edges = append(f.Edges[:k], f.Edges[k+1:]...)
					}
				}
				// Can't form a face if edge number is 1 + 1 < 3
				if len(f.Edges) < 1 {
					mh.Faces = append(mh.Faces[:j], mh.Faces[j+1:]...)
				}
			}

		// If both points are on the negative side, there is no clip
		case u.Loe(d0) && u.Loe(d1):

		// Edge is clipped somewhere in the middle
		default:
			// Find intersection point
			intPoint, _ := p.IntersectWithSegment(e.Vertices[0].Point, e.Vertices[1].Point)

			// Add intersection point to the vertice list
			intVertex := Vertex{intPoint, 0, 0}
			mh.Vertices = append(mh.Vertices, &intVertex)

			// If first vertice is on the positive side than the new edge is [d0, I]
			// Else, d1 > 0 and the new edge is [I, d1]
			if d0 < 0 {
				e.Vertices[1] = &intVertex
			} else if d1 < 0 {
				e.Vertices[0] = &intVertex
			}
		}
	}

	// Process faces
	// Because of the clipping, mesh now has a new face. It needs to be added to the list.
	newFace := Face{Plane: p}

	// After the edge phase is finished, we are left with faces with open edges.
	for _, f := range mh.Faces {
		// Find how many times a vertex exists in a face's vertex list
		for _, e := range f.Edges {
			e.Vertices[0].occurence++
			e.Vertices[1].occurence++
		}

		// Determine the opening points
		var start, end *Vertex
		for _, e := range f.Edges {
			for _, v := range e.Vertices {
				if v.occurence == 1 {
					if start == nil {
						start = v
					} else if end == nil {
						end = v
					}
				}

				// Reset occurences for next time
				v.occurence = 0
			}
		}

		// The face is open
		if start != nil && end != nil {
			// New edge from the points of opening
			newEdge := Edge{[]*Vertex{start, end}}

			// Add the edge to all relevent places
			newFace.Edges = append(newFace.Edges, &newEdge)
			f.Edges = append(f.Edges, &newEdge)
			mh.Edges = append(mh.Edges, &newEdge)
		}
	}

	// Add the new face to the face list
	mh.Faces = append(mh.Faces, &newFace)

	return ClipNormal
}

// Calculate bounding box finds the minimum and maximum points that
// encapsulates the brush.
func (mh *Mesh) CalculateBoundingBox() {
	mh.Box.Min = mh.Vertices[0].Point
	mh.Box.Max = mh.Vertices[0].Point
	for _, v := range mh.Vertices[1:] {
		mh.Box.Min[0] = u.Min(v.Point[0], mh.Box.Min[0])
		mh.Box.Min[1] = u.Min(v.Point[1], mh.Box.Min[1])
		mh.Box.Min[2] = u.Min(v.Point[2], mh.Box.Min[2])
		mh.Box.Max[0] = u.Max(v.Point[0], mh.Box.Max[0])
		mh.Box.Max[1] = u.Max(v.Point[1], mh.Box.Max[1])
		mh.Box.Max[2] = u.Max(v.Point[2], mh.Box.Max[2])
	}
}

// OutputOBJ saves mesh in OBJ format.
func (mh *Mesh) OutputOBJ(filename string) {
	var s string

	// Print all vertices
	for _, v := range mh.Vertices {
		s += fmt.Sprintf("v %f %f %f\n", v.Point[0], v.Point[1], v.Point[2])
	}

	// Find vertice indices for all faces and print them
	for _, f := range mh.Faces {
		s += fmt.Sprintf("f")
		winding := f.Winding()
		for _, p := range winding {
			for i, v := range mh.Vertices {
				if p == v {
					s += fmt.Sprintf(" %v", i+1)
				}
			}
		}
		s += fmt.Sprintf("\n")
	}

	ioutil.WriteFile(filename, []byte(s), os.ModePerm)
}
