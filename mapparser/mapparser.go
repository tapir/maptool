package mapparser

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"

	m "github.com/go-gl/mathgl/mgl32"
)

type BrushPlane struct {
	Points        []m.Vec3 // 3 points of the plane
	Texture       string   // Texture name
	TextureOffset m.Vec2   // Texture offset
	TextureAngle  float32  // Texture angle
	TextureScale  m.Vec2   // Texture scale
	ContentFlag   uint32
	SurfaceFlag   uint32
	Value         int32
}

type ControlPoint struct {
	Coords        m.Vec3 // x, y, z of control point
	TextureCoords m.Vec2 // s, t coords for texture mapping
}

type PatchBrush struct {
	Texture     string // Texture name
	NumOfPoints int
	Columns     []ControlPoint // List of control points for the curve
}

type NormalBrush struct {
	Planes []BrushPlane // Planes of the brush (minimum 4)
}

type Brush struct {
	Data         interface{} // Can be patch or normal brush
	brushCreated bool        // To prevent brush being created again (Normal/Patch thing)
}

type Entity struct {
	Properties map[string]string // Key-value pairs of the entity
	Brushes    []Brush           // Brush definitions if any
}

// QuakeMap holds Q1, Q2 and Q3 maps
type QuakeMap struct {
	Entities []Entity
	log      string
}

type mapLog struct {
	filename      string
	isSuccess     bool
	lastLine      int
	errorMsg      string
	entitiesFound int
	brushesFound  int
	patchesFound  int
	normalsFound  int
}

func (m mapLog) String() string {
	var s string

	s += fmt.Sprintf("Entities found: %v\n", m.entitiesFound)
	s += fmt.Sprintf("Total brushes found: %v\n", m.brushesFound)
	s += fmt.Sprintf("Normal brushes found: %v\n", m.normalsFound)
	s += fmt.Sprintf("Patch brushes found: %v\n", m.patchesFound)

	if m.isSuccess {
		s += fmt.Sprintf("File %v was parsed successfully.", m.filename)
	} else {
		s += fmt.Sprintf("There were errors parsing file %v.\n", m.filename)
		s += fmt.Sprintf("Line: %v -> %v", m.lastLine, m.errorMsg)
	}

	return s
}

func parseNormalBrush(line string, brush *NormalBrush) {
	// Remove '(', ')' and split by space
	rep := strings.NewReplacer("(", "", ")", "")
	params := strings.Fields(rep.Replace(line))
	length := len(params)

	if length < 15 {
		fmt.Println(line)
		fmt.Println(params)
		panic(fmt.Errorf("Illegal brush plane definition"))
	}

	x1, _ := strconv.ParseFloat(params[0], 32)
	y1, _ := strconv.ParseFloat(params[1], 32)
	z1, _ := strconv.ParseFloat(params[2], 32)
	x2, _ := strconv.ParseFloat(params[3], 32)
	y2, _ := strconv.ParseFloat(params[4], 32)
	z2, _ := strconv.ParseFloat(params[5], 32)
	x3, _ := strconv.ParseFloat(params[6], 32)
	y3, _ := strconv.ParseFloat(params[7], 32)
	z3, _ := strconv.ParseFloat(params[8], 32)
	x4, _ := strconv.ParseFloat(params[10], 32)
	y4, _ := strconv.ParseFloat(params[11], 32)
	x5, _ := strconv.ParseFloat(params[12], 32)
	y5, _ := strconv.ParseFloat(params[13], 32)
	an, _ := strconv.ParseFloat(params[14], 32)
	cf := uint64(0) // Default value, can be ommitted
	sf := uint64(0) // Default value, can be ommitted
	va := int64(0)  // Default value, can be ommitted

	if length == 16 {
		cf, _ = strconv.ParseUint(params[15], 10, 32)
	}

	if length == 17 {
		sf, _ = strconv.ParseUint(params[16], 10, 32)
	}

	if length == 18 {
		va, _ = strconv.ParseInt(params[17], 10, 32)
	}

	pl := BrushPlane{
		[]m.Vec3{
			{float32(x1), float32(y1), float32(z1)},
			{float32(x2), float32(y2), float32(z2)},
			{float32(x3), float32(y3), float32(z3)},
		},
		params[9],
		m.Vec2{float32(x4), float32(y4)},
		float32(an),
		m.Vec2{float32(x5), float32(y5)},
		uint32(cf),
		uint32(sf),
		int32(va),
	}

	brush.Planes = append(brush.Planes, pl)
}

func parsePatchBrush(line string, patch *PatchBrush) {
	// Remove '(', ')' and split by space
	rep := strings.NewReplacer("(", "", ")", "")
	params := strings.Fields(rep.Replace(line))

	// Check for single opening/closing paranthesis and ignore
	if line == "(" || line == ")" {
		return
	}

	// If only 5 parameters are parsed, it's column/row numbers
	if len(params) == 5 {
		n, _ := strconv.ParseInt(params[1], 10, 32)
		patch.NumOfPoints = int(n)
		return
	}

	if len(params) != patch.NumOfPoints*5 {
		panic(fmt.Errorf("Illegal patch brush line"))
	}

	for i := 0; i < patch.NumOfPoints; i++ {
		x, _ := strconv.ParseFloat(params[0], 32)
		y, _ := strconv.ParseFloat(params[1], 32)
		z, _ := strconv.ParseFloat(params[2], 32)
		s, _ := strconv.ParseFloat(params[3], 32)
		t, _ := strconv.ParseFloat(params[4], 32)

		cl := ControlPoint{
			m.Vec3{float32(x), float32(y), float32(z)},
			m.Vec2{float32(s), float32(t)},
		}

		patch.Columns = append(patch.Columns, cl)
	}
}

func parseEntityProperty(line string, entity *Entity) {
	props := strings.Split(line, "\"")
	if len(props) < 5 {
		panic(fmt.Errorf("Illegal property line"))
	}
	entity.Properties[props[1]] = props[3]
}

// ParseMapFile parses Quake 1/2/3 entites, entity properties and brushes from *.map files.
func ParseMapFile(filename string) (qmap QuakeMap, err error) {
	var (
		lastEntity *Entity
		lastBrush  *Brush
		lastNormal *NormalBrush
		lastPatch  *PatchBrush
		lineNum    int
		mapLog     = mapLog{filename: filename}
		nesting    = new(stack)
	)

	// Recover all errors and form the correct error string before returning
	defer func() {
		if r := recover(); r != nil {
			e, _ := r.(error)
			mapLog.errorMsg = e.Error()
			err = fmt.Errorf(mapLog.String())
		} else {
			mapLog.isSuccess = true
			qmap.log = mapLog.String()
		}
	}()

	// Open file
	file, err := os.Open(filename)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// Start scanning file line by line
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()

		// Remove any comments
		index := strings.Index(line, "//")
		if index >= 0 {
			line = line[:index]
		}

		// Clean it
		line = strings.TrimSpace(line)

		// Ignore empty lines
		if len(line) == 0 {
			continue
		}

		currentBlock := nesting.peek()
		firstChar := []rune(line)[0]
		lineNum++
		mapLog.lastLine = lineNum

		// Relevent lines goes here
		if line == "{" {
			switch currentBlock {
			case noBlock:
				nesting.push(entityBlock)
				mapLog.entitiesFound++
				qmap.Entities = append(qmap.Entities, Entity{Properties: make(map[string]string, 0)})
				lastEntity = &qmap.Entities[len(qmap.Entities)-1]

			case entityBlock:
				nesting.push(brushBlock)
				mapLog.brushesFound++
				lastEntity.Brushes = append(lastEntity.Brushes, Brush{})
				lastBrush = &lastEntity.Brushes[len(lastEntity.Brushes)-1]

			case brushBlock:
				panic(fmt.Errorf("Can't have a block inside a brush block"))
			}
		} else if line == "}" {
			if currentBlock != noBlock {
				nesting.pop()
			} else {
				panic(fmt.Errorf("No block to exit"))
			}
		} else if firstChar == '"' {
			if currentBlock == entityBlock {
				parseEntityProperty(line, lastEntity)
			} else {
				panic(fmt.Errorf("Property definition outside of entity block"))
			}
		} else if firstChar == '(' {
			switch currentBlock {
			case patchBlock:
				parsePatchBrush(line, lastPatch)

			case brushBlock:
				if !lastBrush.brushCreated {
					mapLog.normalsFound++
					lastNormal = &NormalBrush{}
					lastBrush.Data = lastNormal
					lastBrush.brushCreated = true
				}
				parseNormalBrush(line, lastNormal)
			default:
				panic(fmt.Errorf("Brush line outside of brush block"))
			}
		} else if line == "patchDef2" {
			if !lastBrush.brushCreated {
				mapLog.patchesFound++
				nesting.push(patchBlock)
				lastPatch = &PatchBrush{}
				lastBrush.Data = lastPatch
				lastBrush.brushCreated = true
			}
		} else {
			// If we're in a patch block and the line is not recognized, it's a texture
			if currentBlock == patchBlock {
				lastPatch.Texture = line
			}
			// Ignore everything else (comments, garbage etc ...)
		}
	}
	return
}

func (q QuakeMap) String() string {
	return q.log
}
