package mapparser

type blockType uint

const (
	noBlock blockType = iota
	entityBlock
	brushBlock
	patchBlock
)

type stack struct {
	data []blockType
}

func (s *stack) push(value blockType) {
	s.data = append(s.data, value)
}

func (s *stack) pop() {
	l := len(s.data)
	if l != 0 {
		s.data = s.data[:l-1]
	}
}

func (s *stack) peek() blockType {
	l := len(s.data)
	if l == 0 {
		return noBlock
	}
	return s.data[l-1]
}
