package world

import (
	"log"

	g "gitlab.com/tapir/maptool/geometry"
	p "gitlab.com/tapir/maptool/mapparser"
	u "gitlab.com/tapir/maptool/utils"

	m "github.com/go-gl/mathgl/mgl32"
)

type World struct {
	Brushes   []*g.Mesh
	Triangles []float32
	Indices   []uint32
	Box       g.BoundingBox
}

func NewWorld(filename string) *World {
	newWorld := new(World)

	// Parse the *.map file
	u.StartTimer()
	mapfile, err := p.ParseMapFile(filename)
	if err != nil {
		log.Fatal(err)
	}
	u.StopTimer("Map parsing")

	// Initial world limits
	newWorld.Box.Min = m.Vec3{u.BogusWorldSize, u.BogusWorldSize, u.BogusWorldSize}
	newWorld.Box.Max = newWorld.Box.Min.Mul(-1)

	// Create all the meshes from plane definitions
	u.StartTimer()
	for _, e := range mapfile.Entities {
		for _, b := range e.Brushes {
			switch brush := b.Data.(type) {
			case *p.NormalBrush:
				var result g.ClipResult
				mesh := g.NewCubeMesh(u.MaxWorldSize)

				// For every plane, clip the big mesh
				for _, pl := range brush.Planes {
					result = mesh.Clip(g.NewPlaneFromPoints(pl.Points))

					// If everything get clipped, cancel the brush creation
					if result == g.ClipEverything {
						break
					}
				}

				// Otherwise add the brush to the world
				if result != g.ClipEverything {
					mesh.CalculateBoundingBox()
					newWorld.Brushes = append(newWorld.Brushes, mesh)

					//Calculate world bounding box
					newWorld.Box.Min[0] = u.Min(mesh.Box.Min[0], newWorld.Box.Min[0])
					newWorld.Box.Min[1] = u.Min(mesh.Box.Min[1], newWorld.Box.Min[1])
					newWorld.Box.Min[2] = u.Min(mesh.Box.Min[2], newWorld.Box.Min[2])
					newWorld.Box.Max[0] = u.Max(mesh.Box.Max[0], newWorld.Box.Max[0])
					newWorld.Box.Max[1] = u.Max(mesh.Box.Max[1], newWorld.Box.Max[1])
					newWorld.Box.Max[2] = u.Max(mesh.Box.Max[2], newWorld.Box.Max[2])
				}
			}
		}
	}
	u.StopTimer("Brush generation")

	/* FIXME:	INCREASES VERTICE AND TRIANGLE COUNT (1.5x)
	SOME BRUSHES ARE CLIPPED ALL TOGETHER*/
	// Get rid of intersecting parts by spliting brushes
	u.StartTimer()
	for i := 0; i < len(newWorld.Brushes); i++ {
		master := newWorld.Brushes[i]
		for j := len(newWorld.Brushes) - 1; j > i; j-- {
			slave := newWorld.Brushes[j]
			if master.IsDisjoint(slave) {
				continue
			}
			// Subdivide brush into smaller brushes
			newbrushes := slave.Subtract(master)
			if len(newbrushes) != 0 {
				// Add the new brushes
				newWorld.Brushes = append(newWorld.Brushes, newbrushes...)
				// Remove the original
				newWorld.Brushes = append(newWorld.Brushes[:j], newWorld.Brushes[j+1:]...)
			}
		}
	}
	u.StopTimer("Brush division")

	// TODO: Get rid of all the non-visible faces

	// Triangulate faces and remove duplicate vertices that have the same normal
	newWorld.Triangles, newWorld.Indices = newWorld.indexedTris()

	return newWorld
}

func (w *World) indexedTris() ([]float32, []uint32) {
	var (
		ind      []uint32
		vert     []float32
		tris     []*g.Triangle
		vertices = make(map[*g.Vertex]uint32, 0)
	)

	// Prepare all brushes for OpenGL
	u.StartTimer()
	for _, b := range w.Brushes {
		tris = append(tris, b.Triangles()...)
	}
	u.StopTimer("Triangle generation")

	// Compare all tris
	// ATTENTION: EpsilonVertice makes all the difference
	u.StartTimer()
	countTris := len(tris)
	for i := 0; i < countTris; i++ {
		for j := i + 1; j < countTris; j++ {
			tris[j].Compare(tris[i])
		}
	}
	u.StopTimer("Triangle compare")

	// Create a map of all unique vertices
	// Convert triangles from vertex to indice
	u.StartTimer()
	for _, t := range tris {
		trindices := make([]uint32, 3)
		for j, v := range t.Vertices {
			if val, ok := vertices[v]; ok {
				trindices[j] = val
			} else {
				indice := uint32(len(vertices))
				vertices[v] = indice
				trindices[j] = indice
			}
		}
		ind = append(ind, trindices...)
	}

	// Convert map to vertex slice
	vert = make([]float32, len(vertices)*3)
	for v, i := range vertices {
		vert[i*3], vert[i*3+1], vert[i*3+2] = v.Point.Elem()
	}
	u.StopTimer("Vertex/Index buffer")

	return vert, ind
}
