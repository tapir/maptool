### TODO
* Implement brush overlap occlusion
    - Check every mesh against other meshes for quick overlap or touching - DONE
    - If so split the tested mesh with the planes of the other one - DONE
    - Add the resulting meshes to the world (Resulting meshes are no longer needed to be closed)
* A more general mesh clip() method needed - DONE/NOT_NEEDED
* Mesh split, subtract, crop etc ... (https://github.com/id-Software/Quake-2-Tools/blob/master/bsp/qbsp3/csg.c) - DONE
* Snapping brushes together when they are closer than epsilon?