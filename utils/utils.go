package utils

import m "github.com/go-gl/mathgl/mgl32"

// Approx less then or equal to 0
func Loe(v float32) bool {
	return v < 0 || m.FloatEqualThreshold(v, 0, Epsilon)
}

// Approx more than or equal to 0
func Moe(v float32) bool {
	return v > 0 || m.FloatEqualThreshold(v, 0, Epsilon)
}

// Approx more
func More(a, b float32) bool {
	return !m.FloatEqualThreshold(a, b, Epsilon) && a > b
}

// Approx less
func Less(a, b float32) bool {
	return !m.FloatEqualThreshold(a, b, Epsilon) && a < b
}

// float32 version of math.Min
func Min(a, b float32) float32 {
	if a < b {
		return a
	}
	return b
}

// float32 version of math.Max
func Max(a, b float32) float32 {
	if a > b {
		return a
	}
	return b
}

// float32 version of math.Abs
func Abs(a float32) float32 {
	if a < 0 {
		return -a
	}
	return a
}
