package utils

import (
	"fmt"
	"time"
)

var elapsed int64

func StartTimer() {
	elapsed = time.Now().UnixNano()
}

func StopTimer(title string) {
	elapsed = time.Now().UnixNano() - elapsed
	fmt.Printf("%v: %.3fs\n", title, float64(elapsed)/1000000000)
}
