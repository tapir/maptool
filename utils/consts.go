package utils

const Epsilon float32 = 1e-2
const EpsilonVertice float32 = 1e-4
const BogusWorldSize float32 = 2147483647
const MinGridBox float32 = 4
const MaxWorldSize float32 = 65534
