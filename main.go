// Copyright 2014 The go-gl Authors. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// Renders a textured spinning cube using GLFW 3 and OpenGL 4.1 core forward-compatible profile.
package main

import (
	"fmt"
	"log"
	"math"
	"runtime"
	"strings"

	"io/ioutil"
	"os"

	"github.com/go-gl/gl/v3.3-core/gl"
	"github.com/go-gl/glfw/v3.2/glfw"
	"github.com/go-gl/mathgl/mgl32"
	"github.com/go-gl/mathgl/mgl64"
	u "gitlab.com/tapir/maptool/utils"
	"gitlab.com/tapir/maptool/world"
)

const (
	width       = 1280
	height      = 720
	aspect      = float32(width / height)
	speed       = float32(8)
	sensitivity = float64(0.05)
	scroll      = float64(4)
)

var (
	yaw        = float64(90)
	pitch      = float64(0)
	fov        = float64(45)
	position   = mgl32.Vec3{0, 0, 5}
	direction  = mgl32.Vec3{0, 0, -1}
	up         = mgl32.Vec3{0, 1, 0}
	lastX      = float64(width / 2)
	lastY      = float64(height / 2)
	dt         = float64(0)
	firstMouse = true
)

func move(window *glfw.Window) {
	ss := speed * float32(dt)

	// Move forward
	if window.GetKey(glfw.KeyW) == glfw.Press {
		position = position.Add(direction.Mul(ss))
	}

	// Move backward
	if window.GetKey(glfw.KeyS) == glfw.Press {
		position = position.Sub(direction.Mul(ss))
	}

	// Strafe left
	if window.GetKey(glfw.KeyA) == glfw.Press {
		position = position.Sub(direction.Cross(up).Normalize().Mul(ss))
	}

	// Strafe right
	if window.GetKey(glfw.KeyD) == glfw.Press {
		position = position.Add(direction.Cross(up).Normalize().Mul(ss))
	}
}

func scrollCB(window *glfw.Window, xoff, yoff float64) {
	fov -= yoff * scroll
	if fov <= 25 {
		fov = 25
	}
	if fov >= 110 {
		fov = 110
	}
}

func mouseCB(window *glfw.Window, xpos, ypos float64) {
	if firstMouse {
		lastX = xpos
		lastY = ypos
		firstMouse = false
	}

	xoff := xpos - lastX
	yoff := lastY - ypos
	lastX = xpos
	lastY = ypos

	xoff *= sensitivity
	yoff *= sensitivity

	yaw += xoff
	pitch += yoff

	if pitch > 89 {
		pitch = 89
	}
	if pitch < -89 {
		pitch = -89
	}

	dir := mgl32.Vec3{
		float32(math.Cos(mgl64.DegToRad(yaw)) * math.Cos(mgl64.DegToRad(pitch))),
		float32(math.Sin(mgl64.DegToRad(pitch))),
		float32(math.Sin(mgl64.DegToRad(yaw)) * math.Cos(mgl64.DegToRad(pitch))),
	}
	direction = dir.Normalize()
}

func init() {
	// GLFW event handling must run on the main OS thread
	runtime.LockOSThread()
}

func main() {
	if err := glfw.Init(); err != nil {
		log.Fatalln("failed to initialize glfw:", err)
	}
	defer glfw.Terminate()

	glfw.WindowHint(glfw.Resizable, glfw.False)
	glfw.WindowHint(glfw.ContextVersionMajor, 3)
	glfw.WindowHint(glfw.ContextVersionMinor, 3)
	glfw.WindowHint(glfw.OpenGLProfile, glfw.OpenGLCoreProfile)
	glfw.WindowHint(glfw.OpenGLForwardCompatible, glfw.True)

	window, err := glfw.CreateWindow(width, height, "maptool test", nil, nil)
	if err != nil {
		panic(err)
	}
	window.MakeContextCurrent()

	// Set callback
	window.SetScrollCallback(scrollCB)
	window.SetCursorPosCallback(mouseCB)

	// Hide mouse
	window.SetInputMode(glfw.CursorMode, glfw.CursorDisabled)

	// Initialize gl
	if err := gl.Init(); err != nil {
		panic(err)
	}

	// Configure the vertex and fragment shaders
	program, err := newProgram(vertexShader, fragmentShader)
	if err != nil {
		panic(err)
	}

	// Get map data
	w1 := world.NewWorld("mapfiles/intersecting2.map")
	vertices, indices := w1.Triangles, w1.Indices

	u.StartTimer()
	OutputOBJ("offfiles/test.obj", vertices, indices)
	u.StopTimer("OBJ generation")

	fmt.Println("Vertices:", len(vertices), "Indices:", len(indices))
	os.Exit(-1)

	// Configure the vertex data
	var vao uint32
	gl.GenVertexArrays(1, &vao)
	gl.BindVertexArray(vao)

	var vbo uint32
	gl.GenBuffers(1, &vbo)
	gl.BindBuffer(gl.ARRAY_BUFFER, vbo)
	gl.BufferData(gl.ARRAY_BUFFER, len(vertices)*4, gl.Ptr(vertices), gl.STATIC_DRAW)

	vertAttrib := uint32(gl.GetAttribLocation(program, gl.Str("vert\x00")))
	gl.EnableVertexAttribArray(vertAttrib)
	gl.VertexAttribPointer(vertAttrib, 3, gl.FLOAT, false, 3*4, gl.PtrOffset(0))

	// Index buffer
	var indexb uint32
	gl.GenBuffers(1, &indexb)
	gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexb)
	gl.BufferData(gl.ELEMENT_ARRAY_BUFFER, len(indices)*4, gl.Ptr(indices), gl.STATIC_DRAW)

	// Configure global settings
	gl.Viewport(0, 0, width, height)
	gl.Enable(gl.DEPTH_TEST)
	gl.Enable(gl.CULL_FACE)

	previousTime := glfw.GetTime()
	for !window.ShouldClose() {
		// Update
		time := glfw.GetTime()
		dt = time - previousTime
		previousTime = time

		// Poll events
		glfw.PollEvents()
		move(window)

		// Clear screen
		gl.ClearColor(0.2, 0.3, 0.3, 1)
		gl.Clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT)

		gl.UseProgram(program)

		// Get MVP ID
		projectionid := gl.GetUniformLocation(program, gl.Str("projection\x00"))
		viewid := gl.GetUniformLocation(program, gl.Str("view\x00"))
		modelid := gl.GetUniformLocation(program, gl.Str("model\x00"))

		// Projection matrix
		projection := mgl32.Perspective(mgl32.DegToRad(float32(fov)), aspect, 0.1, 10000.0)
		// Camera matrix
		view := mgl32.LookAtV(
			position,
			position.Add(direction),
			up,
		)
		model := mgl32.Ident4()
		gl.UniformMatrix4fv(projectionid, 1, false, &projection[0])
		gl.UniformMatrix4fv(viewid, 1, false, &view[0])
		gl.UniformMatrix4fv(modelid, 1, false, &model[0])

		gl.BindVertexArray(vao)
		gl.BindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexb)
		gl.DrawElements(gl.TRIANGLES, int32(len(indices)), gl.UNSIGNED_INT, gl.PtrOffset(0))
		gl.BindVertexArray(0)

		// Maintenance
		window.SwapBuffers()
	}
}

func newProgram(vertexShaderSource, fragmentShaderSource string) (uint32, error) {
	vertexShader, err := compileShader(vertexShaderSource, gl.VERTEX_SHADER)
	if err != nil {
		return 0, err
	}

	fragmentShader, err := compileShader(fragmentShaderSource, gl.FRAGMENT_SHADER)
	if err != nil {
		return 0, err
	}

	program := gl.CreateProgram()

	gl.AttachShader(program, vertexShader)
	gl.AttachShader(program, fragmentShader)
	gl.LinkProgram(program)

	var status int32
	gl.GetProgramiv(program, gl.LINK_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetProgramiv(program, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetProgramInfoLog(program, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to link program: %v", log)
	}

	gl.DeleteShader(vertexShader)
	gl.DeleteShader(fragmentShader)

	return program, nil
}

func compileShader(source string, shaderType uint32) (uint32, error) {
	shader := gl.CreateShader(shaderType)

	csources, free := gl.Strs(source)
	gl.ShaderSource(shader, 1, csources, nil)
	free()
	gl.CompileShader(shader)

	var status int32
	gl.GetShaderiv(shader, gl.COMPILE_STATUS, &status)
	if status == gl.FALSE {
		var logLength int32
		gl.GetShaderiv(shader, gl.INFO_LOG_LENGTH, &logLength)

		log := strings.Repeat("\x00", int(logLength+1))
		gl.GetShaderInfoLog(shader, logLength, nil, gl.Str(log))

		return 0, fmt.Errorf("failed to compile %v: %v", source, log)
	}

	return shader, nil
}

func OutputOBJ(filename string, vertices []float32, indices []uint32) {
	var s string

	// Print all vertices
	for i := 0; i < len(vertices); i += 3 {
		s += fmt.Sprintf("v %f %f %f\n", vertices[i], vertices[i+1], vertices[i+2])
	}

	// Print faces
	for i := 0; i < len(indices); i += 3 {
		s += fmt.Sprintf("f %v %v %v\n", indices[i]+1, indices[i+1]+1, indices[i+2]+1)
	}

	s += fmt.Sprintf("\n")

	ioutil.WriteFile(filename, []byte(s), os.ModePerm)
}

var vertexShader = `
#version 330

uniform mat4 projection;
uniform mat4 view;
uniform mat4 model;

layout(location = 0) in vec3 vert;
out float dist;

void main() {
	vec4 pos = view * model * vec4(vert, 1);
	dist = -pos.z;
	gl_Position = projection * pos;
}
` + "\x00"

var fragmentShader = `
#version 330

in float dist;
out vec3 color;

void main() {
	color = vec3(1/dist,0,0);
}
` + "\x00"
